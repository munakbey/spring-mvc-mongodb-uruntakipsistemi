package com.model;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Stok implements Serializable {

	 private static final long serialVersionUID = 1L;
	
	@Id
	Integer stokId;
	Integer stok;
	Integer urunId;
	String urun;
	
	public Stok(){
		super();
	}
	
	public Stok(Integer stokId, Integer stok,Integer urunId,String urun){
		super();
		this.stokId=stokId;
		this.stok=stok;
		this.urunId=urunId;
		this.urun=urun;
	}
	
	public Integer getStokId(){
		return stokId;
	}
	
	public void setStokId(Integer stokId){
		this.stokId=stokId;
	}
	
	public Integer getstok(){
		return stok;
	}
	
	public void setStok(Integer stok){
		this.stok=stok;
	}

	public Integer getUrunId() {
		return urunId;
	}

	public void setUrunId(Integer urunId) {
		this.urunId = urunId;
	}

	public Integer getStok() {
		return stok;
	}

	public String getUrun() {
		return urun;
	}

	public void setUrun(String urun) {
		this.urun = urun;
	}
	
	
	
	
}
