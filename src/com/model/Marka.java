package com.model;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Marka implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	private Integer markaId;
	private String markaAdi;
	
	
	public Marka(){
		super();
	}
	
	public Marka(Integer markaId, String markaAdi){
		super();
		this.markaId=markaId;
		this.markaAdi=markaAdi;
	}

	public Integer getMarkaId() {
		return markaId;
	}

	public void setMarkaId(Integer markaId) {
		this.markaId = markaId;
	}

	public String getMarkaAdi() {
		return markaAdi;
	}

	public void setMarkaAdi(String markaAdi) {
		this.markaAdi = markaAdi;
	}


	
	
}
