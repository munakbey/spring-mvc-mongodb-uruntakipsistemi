package com.model;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Urun implements Serializable{

    private static final long serialVersionUID = 1L;
 
    @Autowired 
    MongoTemplate mongo_template;

	@Id
	private Integer urunId;
	
	@DBRef
	private Marka marka;
		
	public Integer getMarkaId() {
		return markaId;
	}

	public void setMarkaId(Integer markaId) {
		this.markaId = markaId;
	}

	private String urunAdi;	
	public static Integer markaId;
	private Integer stokId;
	private String stok;
	
	
	private String markaAdi;
	
	public Urun(){
		super();
	}
	
	public Urun(Integer urunId, String urunAdi, Integer markaId, Marka marka,Integer stokId,String markaAdi,String stok){
		super();
		this.urunId=urunId;
		this.urunAdi=urunAdi;
		this.markaId=markaId;
		this.marka=marka;
		this.stokId=stokId;
		this.markaAdi=markaAdi;
		this.stok=stok;
	}
	
	public Integer geturunId(){
		return urunId;
	}
	
	public void seturunId(Integer urunId){
		this.urunId=urunId;
	}
	
	public String geturunAdi(){
		return urunAdi;
	}
	public void seturunAdi(String urunAdi){
		this.urunAdi=urunAdi;
	}
	
	public Integer getmarkaId(){
		return markaId;
	}
	public void setmarkaId(Integer markaId){
		this.markaId=markaId;
	}
	
	public Marka getMarka() {
		return marka;
	}

	public void setMarka(Marka marka) {
		this.marka = marka;
	}
	
	public void cekVeri(){
		mongo_template.getDb().getCollection("marka");
	}

	public Integer getStokId() {
		return stokId;
	}

	public void setStokId(Integer stokId) {
		this.stokId = stokId;
	}

	public String getMarkaAdi() {
		return markaAdi;
	}

	public void setMarkaAdi(String markaAdi) {
		this.markaAdi = markaAdi;
	}

	public String getStok() {
		return stok;
	}

	public void setStok(String stok) {
		this.stok = stok;
	}
	
	

}
