package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.MarkaDao;
import com.dao.StokDao;
import com.dao.UrunDao;
import com.dao.UrunDaoImpl;
import com.model.Marka;
import com.model.Stok;
import com.model.Urun;

@Service
public class MainServiceImpl implements MainService {

	@Autowired 
	UrunDao urun_dao;
	
	@Autowired 
	StokDao stok_dao;

	@Autowired 
	MarkaDao marka_dao;
	

	@Override
	public List<Urun> urunList() {
		return urun_dao.urunList();
	}

	@Override
	public void ekleUrun(Urun urun) {
         urun_dao.ekleUrun(urun);	
	}

	@Override
	public void guncelleUrun(Urun urun) {
		urun_dao.guncelleUrun(urun);
		
	}

	@Override
	public void silme(int urunId) {
		urun_dao.silme(urunId);
	}

	
	@Override
	public List<Stok> stokList() {
		return stok_dao.stokList();
	}

	@Override
	public void ekleStok(Stok stok) {
		stok_dao.ekleStok(stok);
	}

	
	@Override
	public List<Marka> markaList() {
		return marka_dao.markaList();
	}

	@Override
	public void ekleMarka(Marka marka) {
		marka_dao.ekleMarka(marka);
	}

	@Override
	public void sil_dene(String marka) {
		marka_dao.sil_dene(marka);
	}

	
	@Override
	public List<Stok> stokGoruntule(String urun) {
		return urun_dao.stokGoruntule(urun);
	}

	@Override
	public List<Stok> filtre(String urunAdi) {
		return stok_dao.filtre(urunAdi);
	}

	@Override
	public void silStok(String urun) {
		stok_dao.silmeStok(urun);
	}
	
}
