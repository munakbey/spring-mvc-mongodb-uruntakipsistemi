package com.service;

import java.util.List;

import com.model.Marka;
import com.model.Stok;
import com.model.Urun;

public interface MainService {
	public List<Urun> urunList();
	public void ekleUrun(Urun urun);
	public void guncelleUrun(Urun urun);
	public void silme(int urunId);
	
	public List<Stok> stokList();
	public void ekleStok(Stok stok);

	public List<Marka> markaList();
	public void ekleMarka(Marka marka);

	public void sil_dene(String marka);
	
	public List<Stok> stokGoruntule(String urun);
	
	public List<Stok> filtre(String urunAdi);
	
	public void silStok(String urun);
	
	
}
