package com.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.model.Marka;
import com.model.Stok;
import com.model.Urun;

import net.sf.jasperreports.engine.JRException;



public interface UrunDao {

	public List<Urun> urunList();
	public void ekleUrun(Urun urun);
	public void guncelleUrun(Urun urun);
	public void silme(Integer markaId);
//	public void stokGoruntule(Urun urun);
	public List<Stok> stokGoruntule(String urun);

}
