package com.dao;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Repository;

import com.model.Marka;
import com.model.Stok;
import com.model.Urun;
@Repository
public class MarkaDaoImpl implements MarkaDao {

	@Autowired
	MongoTemplate mongo_template;
	
	private static final String COLLECTION_NAME="marka";
	
	private UrunDaoImpl urunDao;
	private Urun urun;
	
	
	
	@Override
	public List<Marka> markaList() {

		return  mongo_template.findAll(Marka.class, COLLECTION_NAME);
	}
	
	@Override
	public void ekleMarka(Marka marka) {
		 if(!mongo_template.collectionExists(Marka.class)){
	    	 mongo_template.createCollection(Marka.class);
	     }
	     marka.setMarkaId((int)UUID.randomUUID().getLeastSignificantBits());
			mongo_template.insert(marka, COLLECTION_NAME);
	}

	@Override
	public void sil_dene(String marka) {
		Query query3 = new Query();            
		query3.addCriteria(Criteria.where("markaAdi").is(marka));
		Urun userTest3 = mongo_template.findAndRemove(query3, Urun.class); 
		
		mongo_template.remove(Query.query(Criteria.where("markaAdi").is(marka)), Marka.class); 
	}







}
