
package com.dao;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.model.Marka;
import com.model.Stok;
import com.model.Urun;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.client.MongoDatabase;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

@Repository
public class UrunDaoImpl implements UrunDao{

	static List<Stok> tempStok;
	
	@Autowired
	MongoTemplate mongo_template;
	
	private static final String COLLECTION_NAME = "urun";
	
	@Override
	public List<Urun> urunList() {
	       	return mongo_template.findAll(Urun.class,COLLECTION_NAME);	  
	}

	@Override
	public void ekleUrun(Urun urun) {
		
	   if(!mongo_template.collectionExists(Urun.class)){
			mongo_template.createCollection(Urun.class);
		}
	
	    urun.seturunId((int)UUID.randomUUID().getLeastSignificantBits());
		mongo_template.insert(urun, COLLECTION_NAME);
	
	}

	@Override
	public void guncelleUrun(Urun urun) {
		mongo_template.save(urun);
	}

	@Override
	public void silme(Integer urunId) {
		mongo_template.remove(Query.query(Criteria.where("_id").is(urunId)), Urun.class);
	}

	@Override
	public List<Stok> stokGoruntule(String urun) {
		Query query3 = new Query();            
		query3.addCriteria(Criteria.where("urun").is(urun));
		List<Stok> ListStok=mongo_template.findAll(Stok.class,COLLECTION_NAME);
		 return ListStok;
	}
	








}
