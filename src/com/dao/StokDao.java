package com.dao;

import java.util.List;

import com.model.Stok;



public interface StokDao {

	public List<Stok> stokList();
	public void ekleStok(Stok stok);
	//public void silmeStok(Integer stokId);
	public void silmeStok(String urun);
	public List<Stok> filtre(String urunAdi);
}
