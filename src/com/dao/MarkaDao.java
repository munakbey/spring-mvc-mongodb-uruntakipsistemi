package com.dao;

import java.util.List;

import com.model.Marka;
import com.model.Stok;
import com.model.Urun;

public interface MarkaDao {
	public List<Marka> markaList();
	public void ekleMarka(Marka marka);
	public void sil_dene(String marka);
}
