package com.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.model.Marka;
import com.model.Stok;
import com.model.Urun;

@Repository
public class StokDaoImpl implements StokDao{

	@Autowired
	MongoTemplate mongo_template;
	
	private static final String COLLECTION_NAME="stok";

	@Override
	public List<Stok> stokList() {		
		return mongo_template.findAll(Stok.class, COLLECTION_NAME);
	}

	@Override
	public void ekleStok(Stok stok) {
     if(!mongo_template.collectionExists(Stok.class)){
    	 mongo_template.createCollection(Stok.class);
     }
     stok.setStokId((int)UUID.randomUUID().getLeastSignificantBits());
		mongo_template.insert(stok, COLLECTION_NAME);
	}
	
	@Override
	public List<Stok> filtre(String urunAdi) {
		Query query1 = new Query();
		
		query1.addCriteria(Criteria.where("urun").is(urunAdi));
		List<Stok> stokList= mongo_template.find(query1, Stok.class);
		return stokList;
		
	}

	@Override
	public void silmeStok(String urun) {
		Query query3 = new Query();            
		query3.addCriteria(Criteria.where("urunAdi").is(urun));
		Urun userTest3 = mongo_template.findAndRemove(query3, Urun.class); 
		
		mongo_template.remove(Query.query(Criteria.where("urun").is(urun)), Stok.class); 
	}
	
}
