package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.dao.UrunDao;
import com.model.Marka;
import com.model.Stok;
import com.model.Urun;
import com.service.MainService;

@Controller
public class MainController {
	
	@Autowired
	MainService main_service;
	
	
	@RequestMapping(value={"/","/index"}, method=RequestMethod.GET)
	public ModelAndView defaultPage(){
		ModelAndView my_model=new ModelAndView();
		my_model.addObject("urunList", main_service.urunList());
		my_model.addObject("markaList", main_service.markaList());
		my_model.setViewName("index");		
		return my_model;
	}
	
	@RequestMapping(value="/ekleme_", method=RequestMethod.POST)
	public String ekle(Urun urun){
		main_service.ekleUrun(urun);	
		return ("redirect:/index");
	}
	
	@RequestMapping(value="/guncelleme_", method=RequestMethod.POST)
	public String guncelle(Urun urun){
		main_service.guncelleUrun(urun);
		return("redirect:/index");
	}
	
	@RequestMapping(value="/silme_/{markaId}" , method=RequestMethod.GET)
	public ModelAndView silme(@PathVariable Integer markaId){
		main_service.silme(markaId);
		return new ModelAndView("redirect:/index");
	}
	
//------------------------- index2 ----------------------------------------
	
	@RequestMapping(value={"/index2"}, method=RequestMethod.GET)
	public ModelAndView listele2(){
		ModelAndView my_model=new ModelAndView();	
		my_model.addObject("stokList",main_service.stokList());
		my_model.addObject("urunList", main_service.urunList());
		my_model.setViewName("index2");
		return my_model;
	}
	
	@RequestMapping(value={"/ekle2"} , method=RequestMethod.POST)
	 public String ekle(Stok stok){
		main_service.ekleStok(stok);
		return("redirect:/index2");
	 }
	
	@RequestMapping(value="/silmeStok/{urun}" , method=RequestMethod.GET)
	public ModelAndView silmeStok(@PathVariable String urun){
		main_service.silStok(urun);
		return new ModelAndView("redirect:/index2");
	}
	
//---------------------index3-------------------------------------	
	
	@RequestMapping(value={"/index3"}, method=RequestMethod.GET)
	public ModelAndView listele3(){
		ModelAndView my_model=new ModelAndView();	
	my_model.addObject("markaList",main_service.markaList());
	my_model.addObject("urunList", main_service.urunList());
		my_model.setViewName("index3");
		return my_model;
	}
	
	@RequestMapping(value={"/ekle3"} , method=RequestMethod.POST)
	 public String ekle3(Marka marka){
		main_service.ekleMarka(marka);
		return("redirect:/index3");
	 }
	
	
	@RequestMapping(value="/silmeMarka/{markaAdi}" , method=RequestMethod.GET)
	public ModelAndView silmeMarka(@PathVariable String markaAdi){
		main_service.sil_dene(markaAdi);
		return new ModelAndView("redirect:/index3");
	}
	
//--------------------------------------------------------------------------------------
	
	@RequestMapping(value="/filtreli/{urunAdi}", method=RequestMethod.GET)
	 public ModelAndView filtreli(/*@ModelAttribute(value="stok.urun") */ @PathVariable String urunAdi){
		
		ModelAndView model = new ModelAndView();
		   model.addObject("stokList", main_service.filtre(urunAdi));
		   model.setViewName("index2");
		return model;

	}
}
